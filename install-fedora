#!/bin/sh

dotsync=false
while test $# -gt 0
do
	case "$1" in
		--sync) dotsync=true
			;;
	esac
	shift
done

startsudo() {
    sudo -v 
    if [ $? -eq 1 ]; then
      echo "Exiting"
      exit
    fi

    ( while true; do sudo -v; sleep 50; done; ) &
    SUDO_PID="$!"
    echo "Started sudo loop $SUDO_PID"
    trap stopsudo SIGINT SIGTERM
}

stopsudo() {
    kill "$SUDO_PID"
    trap - SIGINT SIGTERM
    sudo -k
    echo "Stopped sudo loop $SUDO_PID"
}

_create_ssh_key() {
  if [ ! -f $HOME/.ssh/id_ed25519 ]; then
    ssh-keygen -t ed25519 -N '' -f $HOME/.ssh/id_ed25519
  fi
}

_link_dotfiles() {
  cd link/
  rsync -ar . $HOME
  find .[^.]* * -type f -exec ln -svf $PWD/{} $HOME/{} \;
  cd -
}

_copy_dotfiles() {
  cd copy/
  rsync -ar --ignore-existing . $HOME
  cd -
}

_notice_command() {
  message="${1}"
  command="${@:2}"
  tput setaf 5
  echo -n "${message} "
  echo -n "gonna do <${command}>"
  tput sgr0
  ${command} # 1> /dev/null
  tput setaf 2
  echo "done"
  tput sgr0
}

_dnf_install() {
  sudo dnf install -y ${@}
}

_snap_installations() {
  _notice_command "Installing Telegram Desktop" sudo snap install telegram-desktop
  _notice_command "Installing Android Studio" sudo snap install android-studio --classic
  _notice_command "Installing Flutter" sudo snap install flutter --classic
  _notice_command "Installing Google Cloud SDK" sudo snap install google-cloud-sdk --classic
  _notice_command "Installing Google Cloud SDK" sudo snap install spotify
}

_install_utilities() {
  local -a packages; packages=( \
    ## Necessary stuff
    git \
    curl \
    xclip \
    tree \
    htop \
    rofi \
    imwheel \
    # silversearcher-ag \
    ripgrep \
    fd-find \
    duf \
    bat \
    dog \
    kitty \
    alacritty \
    feh \
    maim \
    trash-cli \
    fzf \
    jq \
    thunderbird \
    # davmail \
    zathura \
    nfs-utils \
    ## Video
    # arandr \
    # autorandr \
    ## Audio
    # pavucontrol \
    ## Funsies
    neofetch \
    lolcat \
  )

  _notice_command "Installing with dnf: ${packages[*]}" _dnf_install ${packages[@]}
}

_install_lazygit() {
  sudo dnf copr enable atim/lazygit -y
  _dnf_install lazygit
}

_install_helix() {
  sudo dnf copr enable varlad/helix -y
  _dnf_install helix
}

_install_node() {
  local -a packages; packages=( \
    nodejs \
    npm \
  )

  _notice_command "Installing with dnf: ${packages[*]}" _dnf_install ${packages[@]}
}

_install_zsh() {
  local -a packages; packages=( \
    zsh \
    util-linux-user \
  )

  _notice_command "Installing with dnf: ${packages[*]}" _dnf_install ${packages[@]}

  sudo chsh -s $(which zsh)
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

  git clone 'https://github.com/zsh-users/zsh-autosuggestions.git' $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions
  git clone 'https://github.com/zsh-users/zsh-syntax-highlighting.git' $HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

  mkdir -p "$HOME/.zsh"
  git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
}

_real_install_starship() {
  curl -sS https://starship.rs/install.sh | sh -s -- -y
}

_install_starship() {
  local -a packages; packages=( \
    starship \
  )

  _notice_command "Installing Starship" _real_install_starship
}

_install_code_extensions() {
  cat vscode-extensions.txt | xargs -I {} code --install-extension {}
}

_install_code() {
  sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
  sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
  _dnf_install code
}

_install_codium() {
  _notice_command "Adding key" sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
  _notice_command "Adding repo" _add_codium_repo
  _dnf_install codium
}

_install_codium_extensions() {
  cat codium-extensions.txt | xargs -I {} codium --install-extension {}
}

_add_codium_repo() {
  printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscodium.repo
}

_setup_nvim() {
  _notice_command "Installing Neovim" sudo apt --yes install neovim
  _notice_command "Setting up vim-plug" curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
}

_create_directories() {
  _notice_command "Create git directory" mkdir -p $HOME/git
  _notice_command "Create src directory" mkdir -p $HOME/src
  _notice_command "Create tmp directory" mkdir -p $HOME/tmp
}

_install_picom() {
  _notice_command "Installing build tools" sudo apt install --yes meson ninja-build
  _notice_command "Install extra dependencies" sudo apt install --yes libxext-dev libxcb1-dev libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libxcb-glx0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl1-mesa-dev libpcre2-dev libpcre3-dev libevdev-dev uthash-dev libev-dev libx11-xcb-dev
  _notice_command "Install picom" sh -c 'cd ~/src && git clone https://github.com/yshui/picom && cd picom && git submodule update --init --recursive && meson --buildtype=release . build && ninja -C build && cp build/src/picom /usr/local/'
}

_install_rust() {
  _notice_command "Install rust" _actual_rust_install
}

_actual_rust_install() {
  curl https://sh.rustup.rs -sSf | sh -s -- -y --no-modify-path
}

startsudo

awk -F'=' '/^ID=/ {print tolower($2)}' /etc/*-release 2> /dev/null

if [ "$dotsync" = true ]
then
  _link_dotfiles
  _copy_dotfiles
  stopsudo
  exit 0
fi

## Install basic utilities
# _apt_update
_install_utilities
_create_directories

# ## Create SSH key and clone dotfiles repo
_dnf_install newt # used for showing popup message (whiptail)
_notice_command "Creating SSH key" _create_ssh_key
xclip -sel clip $HOME/.ssh/id_ed25519.pub
whiptail --msgbox "Your SSH key has been copied to clipboard. Add it to your Gitlab account then press [enter]" 20 60
_notice_command "Cloning dotfiles repository" git clone git@gitlab.com:alfonsinbox/dotfiles $HOME/.dotfiles
_notice_command "Changing to dotfiles directory" cd $HOME/.dotfiles

# ## Install cool stuff :)
_install_codium
_install_codium_extensions
# _snap_installations
# _install_i3
_install_zsh
_install_starship
_install_node
# _setup_nvim
# _install_picom
_install_lazygit
_install_helix
_install_rust
# 
# _install_chrome
# 
_link_dotfiles
_copy_dotfiles

stopsudo
