#!/usr/bin/env bash

dotsync=false
while test $# -gt 0
do
	case "$1" in
		--sync) dotsync=true
			;;
	esac
	shift
done

startsudo() {
    sudo -v 
    if [ $? -eq 1 ]; then
      echo "Exiting"
      exit
    fi

    ( while true; do sudo -v; sleep 50; done; ) &
    SUDO_PID="$!"
    echo "Started sudo loop $SUDO_PID"
    trap stopsudo SIGINT SIGTERM
}

stopsudo() {
    kill "$SUDO_PID"
    trap - SIGINT SIGTERM
    sudo -k
    echo "Stopped sudo loop $SUDO_PID"
}

_apt_update() {
  _notice_command "Updating apt" sudo apt-get update
}

_create_ssh_key() {
  if [ ! -f $HOME/.ssh/id_ed25519 ]; then
    ssh-keygen -t ed25519 -N '' -f $HOME/.ssh/id_ed25519
  fi
}

_link_dotfiles() {
  cd link/
  rsync -ar . $HOME
  find .[^.]* * -type f -exec ln -svf $PWD/{} $HOME/{} \;
  cd -
}

_copy_dotfiles() {
  cd copy/
  rsync -ar --ignore-existing . $HOME
  cd -
}

_notice_command() {
  message="${1}"
  command="${@:2}"
  tput setaf 5
  echo -n "${message} "
  tput sgr0
  ${command} 1> /dev/null
  tput setaf 2
  echo "done"
  tput sgr0
}

_snap_installations() {
  _notice_command "Installing Telegram Desktop" sudo snap install telegram-desktop
  _notice_command "Installing Android Studio" sudo snap install android-studio --classic
  _notice_command "Installing Flutter" sudo snap install flutter --classic
  _notice_command "Installing Google Cloud SDK" sudo snap install google-cloud-sdk --classic
  _notice_command "Installing Google Cloud SDK" sudo snap install spotify
}

_install_utilities() {
  _notice_command "Adding lazygit PPA" sudo add-apt-repository -y ppa:lazygit-team/release

  local -a packages; packages=( \
    ## Necessary stuff
    git \
    curl \
    xclip \
    tree \
    htop \
    rofi \
    imwheel \
    silversearcher-ag \
    ripgrep \
    kitty \
    feh \
    maim \
    trash-cli \
    fzf \
    jq \
    lazygit \
    thunderbird \
    davmail \
    zathura \
    nfs-common \
    ## Video
    arandr \
    autorandr \
    ## Audio
    pavucontrol \
    ## Funsies
    neofetch \
    lolcat \
  )

  _notice_command "Installing with apt: ${packages[*]}" sudo apt-get --yes install ${packages[@]}
}

_install_node() {
  local -a packages; packages=( \
    nodejs \
    npm \
  )

  _notice_command "Installing with apt: ${packages[*]}" sudo apt-get --yes install ${packages[@]}

  sudo chsh -s $(which zsh)
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

  git clone https://github.com/zsh-users/zsh-autosuggestions.git $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

  mkdir -p "$HOME/.zsh"
  git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
}

_install_zsh() {
  local -a packages; packages=( \
    zsh \
  )

  _notice_command "Installing with apt: ${packages[*]}" sudo apt-get --yes install ${packages[@]}

  sudo chsh -s $(which zsh)
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

  git clone https://github.com/zsh-users/zsh-autosuggestions.git $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

  mkdir -p "$HOME/.zsh"
  git clone https://github.com/sindresorhus/pure.git "$HOME/.zsh/pure"
}

_install_code_extensions() {
  cat vscode-extensions.txt | xargs -I {} code --install-extension {}
}

_install_code() {
  sudo snap install code --classic
  _notice_command "Installing code extensions" _install_code_extensions
}

_install_i3() {
  local -a packages; packages=( \
    i3-gaps \
    compton \
  )

  _notice_command "Adding PPA for i3-gaps" sudo add-apt-repository -y ppa:kgilmer/speed-ricer
  _apt_update

  _notice_command "Installing with apt: ${packages[*]}" sudo apt-get --yes install ${packages[@]}
}

_install_chrome() {
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  sudo dpkg --skip-same-version -i google-chrome-stable_current_amd64.deb
  rm -f google-chrome-stable_current_amd64.deb
}

_setup_nvim() {
  _notice_command "Installing Neovim" sudo apt --yes install neovim
  _notice_command "Setting up vim-plug" curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
}

_create_directories() {
  _notice_command "Create git directory" mkdir -p $HOME/git
  _notice_command "Create src directory" mkdir -p $HOME/src
}

_install_picom() {
  _notice_command "Installing build tools" sudo apt install --yes meson ninja-build
  _notice_command "Install extra dependencies" sudo apt install --yes libxext-dev libxcb1-dev libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libxcb-glx0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl1-mesa-dev libpcre2-dev libpcre3-dev libevdev-dev uthash-dev libev-dev libx11-xcb-dev
  _notice_command "Install picom" sh -c 'cd ~/src && git clone https://github.com/yshui/picom && cd picom && git submodule update --init --recursive && meson --buildtype=release . build && ninja -C build && cp build/src/picom /usr/local/'
}

_install_rust() {
  _notice_command "Install rust" sh -c 'curl https://sh.rustup.rs -sSf | sh -s -- -y --no-modify-path'
}

startsudo

case awk -F'=' '/^ID=/ {print tolower($2)}' /etc/*-release 2> /dev/null in
  fedora) 
    echo 'hello fedora'
    ;;
  *) 
    echo "hello fedora"
    ;;
esac

# if [ "$dotsync" = true ]
# then
  # _link_dotfiles
  # _copy_dotfiles
  # stopsudo
  # exit 0
# fi
# 
# ## Install basic utilities
# _apt_update
# _install_utilities
# _create_directories
# 
# ## Create SSH key and clone dotfiles repo
# _notice_command "Creating SSH key" _create_ssh_key
# xclip -sel clip $HOME/.ssh/id_ed25519.pub
# _notice_command "Your SSH key has been copied to clipboard. Add it to your Gitlab account then press [enter]" read
# _notice_command "Cloning dotfiles repository" git clone git@gitlab.com:alfonsinbox/ubuntu-dotfiles $HOME/dotfiles
# _notice_command "Changing to dotfiles directory" cd $HOME/dotfiles
# 
# ## Install cool stuff :)
# _install_code
# _snap_installations
# _install_i3
# _install_zsh
# _install_node
# _setup_nvim
# _install_picom
# 
# _install_chrome
# 
# _link_dotfiles
# _copy_dotfiles
# 
stopsudo
