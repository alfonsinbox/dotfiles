#!/usr/bin/env bash

calc() { printf "%s\n" "$*" | bc }

screenshot() { maim -u -s -n -l -c 0.157,0.333,0.466,0.4 }

extensions_count() { find . -type f -name "*.*" | awk -F . '{print $NF}' | sort | uniq -c | sort | awk '{print $2"\t"$1}' }

unzd() {
    if [[ $# != 1 ]]; then echo I need a single argument, the name of the archive to extract; return 1; fi
    target="${1%.zip}"
    unzip "$1" -d "${target##*/}"
}

blur_feh_background() {
    fullfile=$(grep -Poh "'\K[^']+" /home/alfons/.fehbg | xargs echo)
    filename=${fullfile##*/}
    extension=${filename##*.}
    convert $fullfile -blur 0x5 ${XDG_DATA_HOME:-$HOME/.local/share}/wallpapers/blur.png # i3lock only supports PNG ${filename##*.}
}

# fzd - cd to selected directory
fzd() {
  local dir
  dir=$(find ${1:-.} -path '*/\.*' -prune \
                  -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}

# cda - including hidden directories
cda() {
  local dir
  dir=$(find ${1:-.} -type d 2> /dev/null | fzf +m) && cd "$dir"
}

# fh - repeat history
fh() {
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed -E 's/ *[0-9]*\*? *//' | sed -E 's/\\/\\\\/g')
}

# cdf - cd into the directory of the selected file
cdf() {
   local file
   local dir
   file=$(fzf +m -q "$1") && dir=$(dirname "$file") && cd "$dir"
}

resolution() {
    ffprobe -v quiet -print_format json -show_format -show_streams $1 | jq '.streams[] | {width, height}'
}

get_volume() {
	amixer -D pulse get Master | grep -oP "\[\d*%\]" | sed "s:[][%]::g" | head -n1
}

# fda - including hidden directories
fda() {
  local dir
  dir=$(find ${1:-.} -type d 2> /dev/null | fzf +m) && cd "$dir"
}
# cf - fuzzy cd from anywhere
# ex: cf word1 word2 ... (even part of a file name)
# zsh autoload function
cf() {
  local file

  file="$(locate -Ai -0 $@ | grep -z -vE '~$' | fzf --read0 -0 -1)"

  if [[ -n $file ]]
  then
     if [[ -d $file ]]
     then
        cd -- $file
     else
        cd -- ${file:h}
     fi
  fi
}

# fh - repeat history
fh() {
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed -E 's/ *[0-9]*\*? *//' | sed -E 's/\\/\\\\/g')
}
