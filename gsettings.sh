#!/bin/sh

# Set keyboard speed
gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 20
gsettings set org.gnome.desktop.peripherals.keyboard delay 200

# Fix alt+tab behaviour
gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Super>Tab', '<Alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings switch-windows-backward "['<Shift><Super>Tab', '<Shift><Alt>Tab']"
gsettings set org.gnome.desktop.wm.keybindings switch-applications "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-applications-backward "[]"

# Disable screen sleep
gsettings set org.gnome.desktop.session idle-delay 0

# Disable suspend when idle
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'
