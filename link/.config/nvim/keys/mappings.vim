let mapleader=","

" Save file
nmap <silent><leader>s :w<CR>

" Use Ctrl+p for files like in VSCode
nmap <silent><C-p> :Files<CR>

" Search in all files
nmap <silent><C-f> :Rg<CR>

" Toggle File explorer
nmap <F2> :NERDTreeToggle<CR>
