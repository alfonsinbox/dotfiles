#!/bin/sh

set -eu

dpkg -s xclip >/dev/null 2>&1
if [ $? -ne 0 ]; then
  sudo apt install -y xclip
fi

if [ ! -f $HOME/.ssh/id_ed25519 ]; then
  ssh-keygen -t ed25519 -N '' -f $HOME/.ssh/id_ed25519
else
  echo "Key has already been created"
fi
xclip -sel clip $HOME/.ssh/id_ed25519.pub
echo "Key has been copied to clipboard"
